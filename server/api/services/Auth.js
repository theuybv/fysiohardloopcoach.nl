const path = require('path');

require('dotenv').config({path: path.resolve(__dirname, "../../../.env")});


const ManagementClient = require('auth0').ManagementClient;

const AuthenticationClient = require('auth0').AuthenticationClient;

const auth0 = new AuthenticationClient({
  domain: process.env.AUTH0_DOMAIN,
  clientId: process.env.AUTH0_CLIENT_ID,
  clientSecret: process.env.AUTH0_CLIENT_SECRET
});

module.exports = {};
