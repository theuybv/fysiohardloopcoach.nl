const multer = require('multer');
const storage = require('multer-gridfs-storage')({
  url: 'mongodb://localhost:27017/local',
  file: function* (req, file) {
    let counter = 1;
    for (; ;) {
      // variables req and file are automatically reasigned from the array using destructuring assignment
      [req, file] = yield {
        filename: `${file.originalname}_${counter}`,
        metadata: {
          user: req.user.sub
        }
      };
      counter++;
    }
  }
});
const upload = multer({ storage: storage });
const multerGridFsStorage = upload.single('file');


module.exports = multerGridFsStorage;

