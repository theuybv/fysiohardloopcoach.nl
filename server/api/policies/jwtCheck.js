const path = require('path');

require('dotenv').config({path: path.resolve(__dirname, "../../../.env")});

const jwt = require('express-jwt');

const jwtCheck = jwt({
  secret: process.env.AUTH0_CLIENT_SECRET,
  audience: process.env.AUTH0_CLIENT_ID,
});

module.exports = jwtCheck;
