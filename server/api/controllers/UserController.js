/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const path = require('path');

require('dotenv').config({path: path.resolve(__dirname, "../../../.env")});

module.exports = {
  getToken(req, res) {
    var request = require("request");

    var options = {
      method: 'POST',
      url: 'https://' + process.env.AUTH0_DOMAIN + '/oauth/token',
      headers: {'content-type': 'application/json'},
      body:
        {
          grant_type: 'client_credentials',
          client_id: process.env.AUTH0_CLIENT_ID,
          client_secret: process.env.AUTH0_CLIENT_SECRET,
          audience: process.env.AUTH0_API_URL
        },
      json: true
    };

    request(options, function (error, response, body) {
      if (error) res.negotiate(error);

      res.json(body);
    });
  }
};

