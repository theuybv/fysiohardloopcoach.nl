import auth0Config from '../../config/auth0';
import auth0 from 'auth0-js'
import router from '../router';

export default {
  profile: () => JSON.parse(localStorage.getItem('profile')),
  accessToken: () => localStorage.getItem('accessToken'),
  idToken: () => localStorage.getItem('idToken'),
  expireAt: () => localStorage.getItem('expireAt'),
  auth0Manage: (state, getters) => {
    return (getters.profile) ? new auth0.Management({
      domain: auth0Config.domain,
      token: getters.idToken
    }) : null;
  },
  userId: (state, getters) => {
    return (state.profile) ? state.profile.user_id : null;
  },
  isAdmin: (state, getters) => {
    return (getters.profile) ? getters.profile.app_metadata.roles.indexOf('admin') > -1 : false;
  },
  router() {
    return router;
  }
}
