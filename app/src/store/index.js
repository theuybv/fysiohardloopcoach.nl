import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

import getters from './getters';
import mutations from './mutations';
import states from './states';

/*
=============== ACTIONS ===============
 */
import AuthActions from './actions/auth-actions';
import ApiActions from './actions/api-actions';

const store = new Vuex.Store({
  /*
  @todo consider to split up the store in module like auth, and profile
   */
  // modules: {
  //   auth: require('./auth'),
  //   profile: require('./profile'),
  // },
  state: states,
  mutations: mutations,
  getters: getters,
  actions: Object.assign({},
    AuthActions,
    ApiActions
  )
});

export default store
