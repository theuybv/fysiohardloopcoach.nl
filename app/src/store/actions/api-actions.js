import gql from 'graphql-tag'
import {apolloClient} from "../../main"
import Rx from 'rx';
import {
  getUserByAuth0UserIdQuery,
  allClientUsersQuery,
  allTherapistUsersQuery,
  createVideoMutation
} from "../../gql-tags/index";

export default {
  updateUserMetadata({getters, dispatch, commit}, metadata) {
    return new Promise((resolve, reject) => {
      getters.auth0Manage.patchUserMetadata(getters.userId, metadata, function (err, profile) {
        if (err) {
          reject(err);
        } else {
          dispatch('updateAuth0ProfileOnGraphcool', profile)
            .then(() => {
              commit('setProfile', profile);
              resolve(profile);
            });
        }
      });
    });
  },
  getUser({getters, commit}) {
    /**
     * alternative use rx fromNodeCallback
     return Rx.Observable.fromNodeCallback( getters.auth0Manage.getUser)
     */
    return new Promise((resolve, reject) => {
      getters.auth0Manage.getUser(getters.userId, (err, profile) => {
        if (err) {
          reject(err);
        } else {
          commit('setProfile', profile);
          resolve(profile);
        }
      });
    })
  },
  updateAuth0ProfileOnGraphcool({commit, getters}, profile) {
    const getUserByAuth0UserId$ = Rx.Observable.fromPromise(
      apolloClient.query({
        query: getUserByAuth0UserIdQuery,
        variables: {
          auth0UserId: profile.user_id
        }
      }));

    const updateAuth0Profile$ = (res) => {
      const User = res.data.User;
      const USER_ROLE = (profile.app_metadata.roles.indexOf('admin') > -1) ? 'THERAPIST' : 'CLIENT';
      return Rx.Observable.fromPromise(
        apolloClient.mutate({
          mutation: gql `
              mutation updateAuth0Profile($id:ID! $auth0Profile: Json) {
                updateUser(auth0Profile:$auth0Profile, id: $id, userRole: ${USER_ROLE}) {
                  id,
                  auth0Profile
                }
              }
            `,
          variables: {
            auth0Profile: profile,
            id: User.id
          }
        }));
    };

    return new Promise((resolve, reject) => {
      getUserByAuth0UserId$.map(updateAuth0Profile$)
        .subscribe(() => {
          resolve(profile);
        }, (err) => {
          console.error(err);
          reject(err);
        });
    })
  },
  allClientUsers({getters, commit}) {
    apolloClient.query({
      query: allClientUsersQuery
    }).then((res) => {
      commit('setClients', res.data.allUsers);
    })
  },
  allTherapistUsers({getters, commit}) {
    apolloClient.query({
      query: allTherapistUsersQuery
    }).then((res) => {
    })
  },
  publishVideo({getters, commit}, {file, title, description, client, videoType}) {
    /*
    {
      "secret": "cj6whcyi808ok01878tszn1jd",
        "name": "13949614_526176517578183_1393550278_n.mp4",
        "size": 2993901,
        "url": "https://files.graph.cool/cj4x2vtg54mf20175fdlhyl15/cj6whcyi808ok01878tszn1jd",
        "id": "cj6whcymk08ol0187ad356pev",
        "contentType": "video/mp4"
    }
  */
    return new Promise((resolve, reject) => {
      apolloClient.query({
        query: gql`
        query getUserByAuth0Id {
          allUsers(filter:{
            auth0UserId: "${getters.userId}"
          }) {
            id
          }
        }
      `
      }).then(({data: {allUsers}}) => {
        const User = allUsers[0];
        apolloClient.mutate({
          mutation: createVideoMutation,
          variables: {
            title: title,
            description: description,
            fileId: file.id,
            userId: User.id,
            targetUserId: client,
            videoType: videoType
          }
        })
          .then((res) => {
            resolve(res);
          }).catch(reject);
      }).catch(reject);
    })
  }
}
