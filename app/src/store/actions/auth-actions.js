import router from '../../router/index';
import {apolloClient} from "../../main"
import {createUserMutation} from "../../gql-tags/index";

export default {
  onAuthenticationError({}, {lock}) {
    lock.on('authorization_error', (err) => {
      lock.show({
        flashMessage: {
          type: 'error',
          text: err.error_description
        }
      });
    });
  },
  onAuthentication({state, dispatch, commit, getters}, {lock}) {

    if (/access_token/.test(state.route.path)) {
      lock.on('authenticated', (authResult) => {
        lock.getUserInfo(authResult.accessToken, (error, profile) => {
          if (error) {
            return console.error(error);
          }

          let profileData = {authResult, profile};
          commit('setTokenProfile', profileData);

          // try to registered new user if is not existed in graphcool
          apolloClient.mutate({
            mutation: createUserMutation,
            variables: {
              idToken: authResult.idToken,
            }
          })
            .then(() => {
              // user is new to the app, please on board user
              dispatch('updateUserMetadata', {onBoardingCompleted: false})
                .then(x => router.replace({name: 'UserOnboarding'}));
            })
            .catch((error) => {
              if (error.graphQLErrors[0].code === 3023) { //user already exist!
                dispatch('updateAuth0ProfileOnGraphcool', profile);
                if (getters.isAdmin) {
                  router.replace({name: 'AdminDashboard'});
                } else {
                  router.replace({name: 'UserDashboard'});
                }
              }
            })
        });
      })
    }
  },
}
