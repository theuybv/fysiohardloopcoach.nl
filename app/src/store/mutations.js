export default {
  setTokenProfile(state, {authResult, profile}) {
    localStorage.setItem('expireAt', authResult.idTokenPayload.exp * 1000)
    localStorage.setItem('accessToken', authResult.accessToken)
    localStorage.setItem('idToken', authResult.idToken)
    localStorage.setItem('profile', JSON.stringify(profile));
    state.profile = profile;
  },
  setProfile(state, profile) {
    const _profile = Object.assign(
      JSON.parse(localStorage.getItem('profile')),
      profile
    );

    localStorage.setItem('profile', JSON.stringify(_profile));

    state.profile = _profile;
  },
  setClients(state, clients) {
    state.clients = clients;
  }
}
