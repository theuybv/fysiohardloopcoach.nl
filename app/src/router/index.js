import Vue from 'vue'
import Router from 'vue-router'
import {authenticated} from "../plugins/auth"
import store from "../store"

// ===== Application Level =====
import Login from '../components/Login.vue'
import Logout from '../components/Logout.vue'
import EmailVerified from '../components/EmailVerified.vue'
import IndexPage from '../components/IndexPage.vue'

// ===== User Level =====
import User from '../components/user/User.vue'
import UserProfile from '../components/user/UserProfile.vue'
import UserDashboard from '../components/user/UserDashboard.vue'
import PersonalDetails from '../components/user/onboarding/PersonalDetails.vue'
import UserOnboarding from '../components/user/onboarding/UserOnboarding.vue'

// ===== Admin Level =====
import Admin from '../components/admin/Admin.vue'
import AdminProfile from '../components/admin/AdminProfile.vue'
import AdminDashboard from '../components/admin/AdminDashboard.vue'
import AdminUpload from '../components/admin/AdminUpload.vue'

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/access_token=*',
      beforeEnter(to, from, next) {
        next();
      }
    },
    {
      path: '/',
      name: 'IndexPage',
      component: IndexPage,
      beforeEnter(to, from, next) {
        if (authenticated()) {
          if (store.getters.isAdmin) {
            next({name: "AdminDashboard"})
          } else {
            next({name: "UserDashboard"})
          }
        } else {
          // when is not loggedIn we have room to show for example commercial page of this Application
          next();
        }
      }
    },
    {
      path: '/admin',
      component: Admin,
      redirect: {name: 'AdminDashboard'},
      meta: {requiresAuth: true},
      children: [
        {
          path: 'profile',
          name: 'AdminProfile',
          component: AdminProfile
        },
        {
          path: 'dashboard',
          name: 'AdminDashboard',
          component: AdminDashboard
        },
        {
          path: 'upload',
          name: 'AdminUpload',
          component: AdminUpload
        },
      ],
    },
    {
      path: '/user',
      component: User,
      meta: {requiresAuth: true},
      redirect: {name: 'UserDashboard'},
      children: [
        {
          path: 'profile',
          name: 'UserProfile',
          component: UserProfile
        },
        {
          path: 'dashboard',
          name: 'UserDashboard',
          component: UserDashboard
        },
      ],
    },
    {
      path: '/intro',
      name: 'UserOnboarding',
      component: UserOnboarding,
      redirect: {name: 'PersonalDetails'},
      beforeEnter(to, from, next) {
        if (store.getters.profile.user_metadata.onBoardingCompleted) {
          if (store.getters.isAdmin) {
            next({name: 'AdminDashboard'})
          } else {
            next({name: 'UserDashboard'})
          }
        }
      },
      meta: {requiresAuth: true},
      children: [
        {
          name: 'PersonalDetails',
          path: 'personal-details',
          component: PersonalDetails
        }
      ]
    },
    {
      path: '/email-verified',
      name: 'EmailVerified',
      component: EmailVerified
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/logout',
      name: 'Logout',
      component: Logout
    },
    {
      path: '*',
      component: Vue.component('errorPage', {
        template: `<h1 style="color: white;">Page not found!</h1>`
      })
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!authenticated()) {
      next('/login')
    } else {
      next();
    }
  } else {
    next() // make sure to always call next()!
  }
});

export default router
