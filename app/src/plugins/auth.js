import Auth0Lock from 'auth0-lock'
import auth0Config from '../../config/auth0'
import axios from 'axios';

const lock = new Auth0Lock(auth0Config.clientID, auth0Config.domain, {
  audience: auth0Config.audience,
  responseType: auth0Config.responseType,
  scope: auth0Config.scope,
  closable: false,
  language: 'nl',
  loginAfterSignUp: true,
  auth: {
    autoParseHash: true,
    // redirectUrl: auth0Config.redirectUrl,
  }
  /* additionalSignUpFields: [{
     type: "select",
     name: "location",
     placeholder: "choose your location",
     options: [
       {value: "us", label: "United States"},
       {value: "fr", label: "France"},
       {value: "ar", label: "Argentina"}
     ],
     // The following properties are optional
     icon: "https://example.com/assests/location_icon.png",
     prefill: "us"
   }]*/
});

export const authenticated = () => {
  const isNotExpired = (new Date().getTime() < parseInt(localStorage.getItem('expireAt')));
  const localStorageExist = (
    localStorage.getItem('idToken')
    && localStorage.getItem('accessToken')
    && localStorage.getItem('profile')
    && localStorage.getItem('expireAt')
  );
  return (localStorageExist && isNotExpired);
};

const auth0Api = axios.create({
  baseURL: `https://${auth0Config.domain}/api/v2/`
});

export default function (Vue, options) {
  // 1. add global method or property
  Vue.authenticated = authenticated;
  // 3. inject some component options
  Vue.mixin({
    created() {
      if(this.authenticated()) {
        this.auth0Api.defaults.headers.common['Authorization'] = `Bearer ${this.$store.getters.accessToken} `;
        // this.auth0Api.defaults.headers.contentType = `application/json`;
      }
    },
    data() {
      return {
        lock,
        auth0Config,
        auth0Api
      }
    },
    methods: {

      login() {
        return lock.show()
      },
      logout() {
        localStorage.removeItem('accessToken');
        localStorage.removeItem('idToken');
        localStorage.removeItem('profile');
        localStorage.removeItem('expireAt');
      },
      authenticated: authenticated
    }
  });
};

