import graphcoolConfig from '../../config/graphcool'

export default function (Vue, options) {
  Vue.mixin({
    data() {
      return {
        graphcoolConfig
      }
    },
    created() {

    }
  })
}
