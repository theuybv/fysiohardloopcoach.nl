// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import store from './store';
import AuthPlugin from './plugins/auth';
import GraphcoolPlugin from './plugins/graphcool';

import {ApolloClient, createBatchingNetworkInterface} from 'apollo-client'
import VueApollo from 'vue-apollo'

// Create the apollo client
export const apolloClient = new ApolloClient({
  networkInterface: createBatchingNetworkInterface({
    uri: 'https://api.graph.cool/simple/v1/cj4x2vtg54mf20175fdlhyl15',
  }),
  connectToDevTools: true,
});

const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
});

// Install the vue plugin
Vue.use(VueApollo);

Vue.config.productionTip = false;

import {sync} from 'vuex-router-sync'


sync(store, router); // done.

Vue.use(AuthPlugin);
Vue.use(GraphcoolPlugin);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  apolloProvider,
  render: h => h(App)
});
