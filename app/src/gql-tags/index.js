import gql from 'graphql-tag'

export const createUserMutation = (() => {
  return gql`
  mutation createUser($idToken:String!) {
    createUser(authProvider: {
      auth0: {
        idToken: $idToken
      }
    }) {
      id
      auth0UserId
    }
  }
`;
})();
export const getUserByAuth0UserIdQuery = (() => {
  return  gql`
  query getUserByAuth0UserId($auth0UserId: String!) {
    User(auth0UserId: $auth0UserId) {
      id
    }
  }
`
})();
export const allClientUsersQuery = (() => {
  return gql`
    query allClientUsers {
      allUsers(filter:{
        userRole: CLIENT
      }) {
        id,
        auth0Profile
      }
  }
  `
})();
export const allTherapistUsersQuery = (() => {
  return gql`
    query allTherapistUsers {
      allUsers(filter:{
        userRole: THERAPIST
      }) {
        id,
        auth0Profile,
        auth0UserId
      }
  }
  `
})();
export const createVideoMutation = (() => {
  return gql`
   mutation createVideo(
      $title:String!, $description:String!, $fileId:ID!, $userId:ID!,
      $targetUserId: String, $videoType: VideoType
    ) {
      createVideo(title: $title, 
        description:$description, 
        fileId: $fileId, 
        userId: $userId,
        targetUserId: $targetUserId,
        type:$videoType) {
        id
        title
        description
        file {
          name
          url
        }
      }
    }
  `
})();
