export default {
  domain: 'tlimpanont.eu.auth0.com',
  clientID: '6o05ZsrqdaaKKctm41It6jxwt2o63ej5',
  redirectUrl: 'http://localhost:3000',
  audience: 'https://tlimpanont.eu.auth0.com/userinfo',
  responseType: 'token id_token access_token',
  scope: 'openid user_metadata app_metadata'
}
