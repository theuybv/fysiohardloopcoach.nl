require('dotenv').config();

const axios = require('axios');

const graphCoolHeaders = {
  headers: {
    'Authorization': `Bearer ${process.env.GRAPHCOOL_API_TOKEN}`
  }
};

const auth0Headers = {
  headers: {
    'Authorization': `Bearer ${process.env.AUTH0_API_TOKEN}`
  }
};

const graphCool = {
  deleteAllUsers: function () {
    const self = this;
    axios.post(process.env.GRAPHCOOL_API_URL, {
      query: `
       query getAllUsers  {
        allUsers{
          id
          auth0UserId
        }
      }
    `
    }).then(function (res) {
      const allUsers = res.data.data.allUsers;
      console.log('allGraphCoolUsers: ' + JSON.stringify(allUsers, null, 4));
      allUsers.forEach(function (user) {
        self.deleteUser(user.id);
      });
    }).catch(console.log);
  },
  deleteUser: function (userId) {
    axios.post(process.env.GRAPHCOOL_API_URL, {
      query: `
      mutation deleteUser {
        deleteUser(id: "${userId}") {
          id
          auth0UserId
        }
      }
  `
    }, graphCoolHeaders).then(function (res) {
      console.log(JSON.stringify(res.data, null, 4));
    }).catch(console.log);

  }
};

const auth0 = {
  deleteAllUsers: function () {
    const self = this;
    axios.get(`${process.env.AUTH0_API_URL}/users`, auth0Headers).then(function (res) {
      const allUsers = res.data;
      console.log('auth0Users: ' + JSON.stringify(allUsers, null, 4));
      allUsers.forEach(function (userId) {
        self.deleteUser(userId);
      });
    }).catch(console.log);
  },
  deleteUser: function (user) {
    axios.delete(`${process.env.AUTH0_API_URL}/users/${user.user_id}`, auth0Headers).then(function (res) {
      console.log(JSON.stringify(res.data, null, 4));
    }).catch(console.log);
  }
};

graphCool.deleteAllUsers();
auth0.deleteAllUsers();
